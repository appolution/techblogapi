module.exports = {
  db: {
    url: 'mongodb://54.252.41.165:27017/tech-blog',
    options: { useNewUrlParser: true, useUnifiedTopology: true },
  },
  port: 4000,
};
