const Koa = require('koa');

const app = new Koa();
const bodyParser = require('koa-bodyparser');
const config = require('../conf');
const { loadModel } = require('./model-route/loader');
const restful = require('./model-route/router');

loadModel(config)(app);
app.use(bodyParser());
app.use(restful);

const port = 3000;
app.listen(port, () => {
  console.log(`app start at port ${port}`);
});
