module.exports = {
  async init(ctx, next) {
    const model = ctx.app.$model[ctx.params.model];
    if (model) {
      ctx.model = model;
      await next();
    } else {
      ctx.body = 'the model does not exist';
    }
  },

  async list(ctx) {
    let {
      page = 1,
      pageSize = 0,
    } = ctx.request.query;
    const {
      query,
      sort = { _id: -1 },
    } = ctx.request.query;
    page = parseInt(page, 10);
    pageSize = parseInt(pageSize, 9);
    if (pageSize === 0) {
      const data = await ctx.model.find(query ? JSON.parse(query) : {}).sort(sort);
      ctx.body = {
        code: 200,
        message: 'OK',
        data,
      };
    } else {
      const result = await ctx.model.find(query).sort(sort).skip((page - 1) * pageSize).limit(pageSize);
      const count = await ctx.model.count();
      // eslint-disable-next-line no-unused-vars
      const data = { page, totalPage: Math.ceil(count / pageSize) };
      data[ctx.params.model] = result;
      ctx.body = {
        code: 200,
        message: 'OK',
        data,
      };
    }
  },
  async get(ctx) {
    const data = await ctx.model.findOne({ _id: ctx.params.id });
    ctx.body = {
      code: 200,
      message: 'OK',
      data,
    };
  },
  async create(ctx) {
    await ctx.model.insertMany(ctx.request.body);
    ctx.body = {
      code: 200,
      message: 'OK',
    };
  },
  async update(ctx) {
    await ctx.model.updateOne({ _id: ctx.params.id }, ctx.request.body);
    ctx.body = {
      code: 200,
      message: 'OK',
    };
  },
  async del(ctx) {
    ctx.body = await ctx.model.deleteOne({ _id: ctx.params.id });
  },
};
