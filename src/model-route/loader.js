const fs = require('fs');
const path = require('path');
const mongoose = require('mongoose');

function load(dir, cb) {
  const url = path.resolve(__dirname, dir);
  const files = fs.readdirSync(url);
  files.forEach((fileName) => {
    const fileNameWithoutExt = fileName.replace('.js', '');
    // eslint-disable-next-line global-require,import/no-dynamic-require
    const file = require(`${url}/${fileNameWithoutExt}`);
    cb(fileNameWithoutExt, file);
  });
}

const loadModel = (config) => (app) => {
  mongoose.connect(config.db.url, config.db.options, null);
  const conn = mongoose.connection;
  conn.on('error', () => console.error('db connection error'));
  // eslint-disable-next-line no-param-reassign
  app.$model = {};
  load('../model', (filename, { schema }) => {
    // eslint-disable-next-line no-param-reassign
    app.$model[filename] = mongoose.model(filename, schema);
  });
};

module.exports = {
  loadModel,
};
