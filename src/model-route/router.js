const router = require('koa-router')();
const {
  // eslint-disable-next-line no-unused-vars
  init, get, create, update, del, list, page,
} = require('./api');

router.get('/api/:model/list', init, list);
router.get('/api/:model/:id', init, get);
router.post('/api/:model', init, create);
router.put('/api/:model/:id', init, update);
router.delete('/api/:model/:id', init, del);

module.exports = router.routes();
