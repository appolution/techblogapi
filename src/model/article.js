module.exports = {
  schema: {
    title: { type: String },
    titleEn: { type: String },
    tags: { type: Array },
    content: { type: String },
    contentEn: { type: String },
    brief: { type: String },
    briefEn: { type: String },
    watch: { type: Number },
    comment: { type: Number },
    like: { type: Number },
    postDate: { type: Number },
    updateDate: { type: Number },
  },
};
