const Koa = require('koa');
const cors = require('koa2-cors');

const app = new Koa();
const bodyParser = require('koa-bodyparser');
const config = require('./conf');
const { loadModel } = require('./src/model-route/loader');
const restful = require('./src/model-route/router');

loadModel(config)(app);
app.use(cors());
app.use(bodyParser());
app.use(restful);

const { port } = config;
app.listen(port, () => {
  console.log(`app start at port ${port}`);
});
